const knexfile = require('../../knexfile');

function connect(env) {
    return require('knex')(knexfile[env]);
}

module.exports = { connect };
