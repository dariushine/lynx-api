const userService = require('./userService');

async function createUser(req, res) {
    try {
        const { username, email, password } = req.body;
        const user = await userService.createUser(username, email, password);
        res.status(201).json(user);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

async function getUsers(req, res) {
    try {
        const users = await userService.getUsers();
        res.json(users);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

async function getUserById(req, res) {
    try {
        const id = req.params.id;
        const user = await userService.getUserById(id);
        if (!user) {
            res.status(404).json({ message: 'User not found' });
        } else {
            res.json(user);
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

module.exports = { createUser, getUsers, getUserById };