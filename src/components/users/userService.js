const User = require('./userModel');

async function createUser(username, email, password) {
  try {
    const user = await User.query().insert({ username, email, password });
    return user;
  } catch (error) {
    return error;
  }
}

async function getUsers() {
  try {
    const users = await User.query().select('id', 'username', 'email',
      'created_at', 'updated_at', 'last_seen');
    return users;
  } catch (error) {
    return error;
  }
}

async function getUserById(id) {
  try {
    const user = await User.query().findById(id).select('id', 'username', 'email',
      'created_at', 'updated_at', 'last_seen');
    return user;
  } catch (error) {
    return error;
  }
}

module.exports = { createUser, getUsers, getUserById };