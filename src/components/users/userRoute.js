const express = require('express');
const router = express.Router();
const { createUser, getUsers, getUserById } = require('./userController');

// Route to create a new user
router.post('/', createUser);

// Route to get all users
router.get('/', getUsers);

// Route to get one user
router.get('/:id', getUserById);

module.exports = router;