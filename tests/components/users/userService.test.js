const UserModel = require('@users/userModel');
const { createUser, getUsers, getUserById } = require('@users/userService');

jest.mock('@users/userModel', () => ({
  query: jest.fn(),
}));

afterEach(() => {
  jest.clearAllMocks();
});

describe('getUsers', () => {
  it('should get a list of all the users', async () => {
    const mockUsers = [{
      username: 'testuser',
      email: 'testemail@example.com',
      password: 'testpassword',
    }]

    const mockQuery = {
      query: jest.fn().mockReturnThis(),
      select: jest.fn().mockReturnValueOnce(mockUsers)
    }

    UserModel.query.mockReturnValueOnce(mockQuery);

    const users = await getUsers();
    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(users).toEqual(mockUsers);
  });

  it('should return an error when it fails to get users', async () => {
    const mockError = new Error('Database error');

    const mockQuery = {
      query: jest.fn().mockReturnThis(),
      select: jest.fn().mockReturnValueOnce([])
    }

    UserModel.query.mockReturnValueOnce(mockQuery);

    const user = await getUsers();

    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(mockQuery.select).toHaveBeenCalledTimes(1);
    expect(user).toEqual([]);
  });
});

describe('getUserById', () => {
  it('should return the user corresponding to an id', async () => {
    const mockUser = {
      username: 'testuser',
      email: 'testemail@example.com',
      password: 'testpassword',
    };

    const userId = 1;

    const mockQuery = {
      findById: jest.fn().mockReturnThis(),
      select: jest.fn().mockReturnValueOnce(mockUser)
    }

    UserModel.query.mockReturnValueOnce(mockQuery);

    const user = await getUserById(userId);

    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(mockQuery.findById).toHaveBeenCalledTimes(1);
    expect(mockQuery.findById).toHaveBeenCalledWith(userId);
    expect(mockQuery.select).toHaveBeenCalledTimes(1);
    expect(user).toEqual(mockUser);
  });

  it('should return an error when it fails to find the user', async () => {
    const userId = 1;

    const mockQuery = {
      findById: jest.fn().mockReturnThis(),
      select: jest.fn().mockReturnValueOnce(null)
    }

    UserModel.query.mockReturnValueOnce(mockQuery);

    const user = await getUserById(userId);

    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(mockQuery.findById).toHaveBeenCalledTimes(1);
    expect(mockQuery.findById).toHaveBeenCalledWith(userId);
    expect(user).toBeNull();
  });
});

describe('createUser', () => {
  it('should insert a new user into the database', async () => {
    const mockUser = {
      username: 'testuser',
      email: 'testemail@example.com',
      password: 'testpassword',
    };

    const mockQuery = {
      insert: jest.fn().mockReturnValueOnce(mockUser),
    };

    UserModel.query.mockReturnValueOnce(mockQuery);

    const user = await createUser(mockUser.username, mockUser.email, mockUser.password);

    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(mockQuery.insert).toHaveBeenCalledTimes(1);
    expect(mockQuery.insert).toHaveBeenCalledWith(mockUser);
    expect(user).toEqual(mockUser);
  });

  it('should return an error if the user insertion fails', async () => {
    const mockError = new Error('Database error');

    const mockQuery = {
      insert: jest.fn().mockRejectedValueOnce(mockError),
    };

    UserModel.query.mockReturnValueOnce(mockQuery);

    const user = await createUser('testuser', 'testemail@example.com', 'testpassword');

    expect(UserModel.query).toHaveBeenCalledTimes(1);
    expect(UserModel.query).toHaveBeenCalledWith();
    expect(mockQuery.insert).toHaveBeenCalledTimes(1);
  });
});